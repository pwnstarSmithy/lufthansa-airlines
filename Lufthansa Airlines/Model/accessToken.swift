//
//  accessToken.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 10/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct token : Decodable {
    let access_token : String
    let token_type : String
    let expires_in : Int
}

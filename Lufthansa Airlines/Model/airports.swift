//
//  airports.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 10/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct AirportData : Decodable {
    let AirportResource : AirportDetails
}

struct AirportDetails : Decodable {
    let Airports : AirportsData
}
struct AirportsData : Decodable {
    let Airport : [Airport]
}

struct Airport: Decodable {
    let AirportCode : String?
    let Position : Coordinate
    let CityCode : String?
    let CountryCode : String?
    let LocationType : String?
    let UtcOffset : Float?
    let TimeZoneId : String?
}

struct Coordinate : Decodable {
    let Latitude : Double?
    let Longitude : Double?
}

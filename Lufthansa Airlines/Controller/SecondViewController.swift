//
//  SecondViewController.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 11/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import SVProgressHUD
//empty variables to save info
var schedulePulled : ScheduleDetails?

class SecondViewController: UIViewController {

    
    
    var originVar : String?
    var destinationVar: String?
    
    var flightType : Int?
    var flightDate : String?
    

    
    //Date picker View
    
    @IBOutlet weak var flightDatePicker: UIDatePicker!
    
    //flight type switch
    
    @IBAction func flightSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true){
            
            flightType = 1
            
        }else{
            flightType = 0
            
        }
        
        
    }
    
    //button to check flights
    @IBAction func checkFlightPressed(_ sender: Any) {
        
        
        //format data
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = formatter.string(from: flightDatePicker.date)
        
        
        flightDate = dateString
        
        print("your flight is on \(flightDate!)")
        print("Do you want to display direct flights only? \(flightType!)")
        
        DispatchQueue.main.async {
            self.fetchFlights()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flightType = 1

        //function to set color of picker view!
        flightDatePicker.setValue(UIColor.white, forKey: "textColor")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchFlights() {
        SVProgressHUD.show(withStatus: "Firing up engines..")
        //url to send the request to
        let url = NSURL(string: "https://api.lufthansa.com/v1/operations/schedules/\(originVar!)/\(destinationVar!)/\(flightDate!)?directFlights=\(flightType!)")!
        print(url)
        
        //append access token to bearer string
        let tokenString = "Bearer " + accessToken!
        
        //request for url
        var request = URLRequest(url: url as URL)
        
        //set authorization parameter in authorization header
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
//        //set parametrs to variable body
//        let body = "origin=\(originVar!)&destination\(destinationVar!)&fromDateTime\(flightDate!)&?directFlights=\(flightType!)"
//
//
//        //set character type for the body
//        request.httpBody = body.data(using: String.Encoding.utf8)
//
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            //check for any errors
            
            if error == nil {
                
                do {
                    
                    let scheduleDetails = try JSONDecoder().decode(scheduleList.self, from: data)
                    
                    
                    schedulePulled = scheduleDetails.ScheduleResource
            
                  
                    
                    //dismiss activity indicator
                    SVProgressHUD.dismiss()
//                    print(schedulePulled)
                    
                    let successAlert = UIAlertController(title: "Flight Schedules", message: "We've successfully found some matching schedules based on your selections!", preferredStyle: UIAlertControllerStyle.alert)
                    
                    successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        self.performSegue(withIdentifier: "toTable", sender: nil)
                        
                        
                    }))
                    
                    DispatchQueue.main.async(execute: {
                        self.present(successAlert, animated: true, completion: nil)
                    })
                    
                    
                }catch{
                    //dismiss activity indicator
                    SVProgressHUD.dismiss()
                    print(error)
                    
                    let failAlert = UIAlertController(title: "Error", message: "Failed to find any direct flights, please try again with different selections or try flights with stops", preferredStyle: UIAlertControllerStyle.alert)
                    
                    failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        failAlert.dismiss(animated: true, completion: nil)
                        
                        
                    }))
                    
                    DispatchQueue.main.async(execute: {
                        self.present(failAlert, animated: true, completion: nil)
                    })
                    
                }
                
                
            }else{
                //dismiss activity indicator
                SVProgressHUD.dismiss()
                
                let failAlert = UIAlertController(title: "Error", message: "Failed to find any schedules, please try again with different selections", preferredStyle: UIAlertControllerStyle.alert)
                
                failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                    
                    failAlert.dismiss(animated: true, completion: nil)
                    
                    
                }))
                
                DispatchQueue.main.async(execute: {
                    self.present(failAlert, animated: true, completion: nil)
                })
                
            }
            
        }.resume()
        
    }
    
    //seguename #toDate



    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? FlightsTableViewController {
            
            destination.flightSchedule = schedulePulled
        
            //send destination and origin variables to next screen
            destination.originVar = originVar
            destination.destinationVar = destinationVar
            
//            destination.flightArray = sc
            
            let flightNames = "\(originVar!) - \(destinationVar!)"
            
            destination.flightName = flightNames
        }
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    
    

}

//
//  FlightsTableViewController.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 13/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class FlightsTableViewController: UITableViewController {

    var flightSchedule : ScheduleDetails?
    
    var flightArray = [ScheduleArray]()
    
    var departAirport : DepartureDetails?
    var arrivalAirport : ArrivalDetails?
    
    var mainArray = [moreFLight]()
    var journeyString : JourneyDetails?
    var flightDate : dateDeparture?
    var flightName : String?
    
    //variables to get airport longitude and latitude
    var airportSmithy : airportData?
    var airportAma : moreAirport?
    var airportSammy : singleAirport?
    var airportPosition : positionDetails?
    var airportCoordinate : cordinateDetails?
    
    
    var airportLatitude : Double?
    var airportLongitude : Double?
    
    var airportLatitudeOrigin : Double?
    var airportLongitudeOrigin : Double?
    
    var airportLatitudeDestination : Double?
    var airportLongitudeDestination : Double?
    
    //variables to hold destination and origin
    var originVar : String?
    var destinationVar : String?
    
    var airportCode : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
        
        
        //call functions to get position details
        
        getOriginPosition()
        getDestinationPosition()
        
        //append flight array to empty array flightarray
        
        
        for ama in (flightSchedule?.Schedule)! {
            
            
            
            flightArray.append(ama)
            
         
         
        }
        
//            for dummyArray in self.flightArray {
//
//                self.mainArray = dummyArray.Flight
//
//
//
//
//            }
        
//
//        for nextArray in self.mainArray{
//
////
////
////            self.departAirport = nextArray.Departure
////            self.arrivalAirport = nextArray.Arrival
////
//            emptyDict[(nextArray.Departure?.AirportCode)!] = nextArray.Arrival?.AirportCode
//
//            dump(emptyDict)
//
//
//        }
    
        
       
        
//        flightArray = flightSchedule?.Schedule
//        print("Starts here!")
//        print(flightArray)
        
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return flightArray.count
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flightList", for: indexPath) as! FlightTableViewCell
        
        cell.FlightName.text = flightName
        cell.FlightName.textColor = UIColor.white
  
        
        if flightArray[indexPath.row].Flight.isEmpty {
            
            print("Nothing to see here, move along!")
        }else{
            
            mainArray = flightArray[indexPath.row].Flight
            
            journeyString = flightArray[indexPath.row].TotalJourney
            
//            print(mainArray)
            
            for dummyArray in mainArray {
                
                departAirport = dummyArray.Departure
                arrivalAirport = dummyArray.Arrival
//                cell.flightStops.text = departAirport?.AirportCode
                
//
              flightDate = departAirport?.ScheduledTimeLocal
    
         
            }
            
        }
       
        //functions to remove characters from the time variable until it has format we want to present to user.
        let durationDummy = journeyString?.Duration
      
        let swiftyTime = durationDummy?.replacingOccurrences(of: "H", with: " Hours ")
        
        let minuteTime = swiftyTime?.replacingOccurrences(of: "M", with: " Minutes.")
        
        let minusPT = minuteTime?.replacingOccurrences(of: "PT", with: "")
        
        let minusP = minusPT?.replacingOccurrences(of: "P", with: "")
        
        let minusD = minusP?.replacingOccurrences(of: "D", with: " Day ")
      
        
        cell.flightDuration.text = minusD
        cell.flightDuration.textColor = UIColor.white
        
        cell.flightStops.text = "\(mainArray.count) Airport Stops"
        cell.flightStops.textColor = UIColor.white
        
        // configure the date returned into a desired format!
        let dateString = flightDate?.DateTime
        
        let dateSwify = dateString?.replacingOccurrences(of: "T", with: " at ")
        
        cell.flightDate.text = dateSwify
        cell.flightDate.textColor = UIColor.white
        
//        cell.flightStops.text = " From \(departAirport?.AirportCode!) To \(arrivalAirport?.AirportCode!)\n"

        

        
        //
//        cell.flightStops.text = departAirport?.AirportCode
//
//        let amaArray = mainArray[indexPath.row].Departure
//
//        cell.flightStops.text = amaArray?.AirportCode
        
//        print(departAirport?.AirportCode)
        
//        dump(departAirport)
//        dump(arrivalAirport)
        // Configure the cell...

        return cell
    }
    
    func getAirportDetails() {
        
        //url to send the request to
        let url = NSURL(string: "https://api.lufthansa.com/v1/references/airports/\(airportCode!)")!
        print(url)
        
        //append access token to bearer string
        let tokenString = "Bearer " + accessToken!
        
        //request for url
        var request = URLRequest(url: url as URL)
        
        //set authorization parameter in authorization header
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            //check for any errors
            
            if error == nil {
                
                do {
                    
                    let pulledAirport = try JSONDecoder().decode(airportInformation.self, from: data)
                    
//                    print(pulledAirport)
                    
                    self.airportSmithy = pulledAirport.AirportResource
                    
                    self.airportAma = self.airportSmithy?.Airports
                    
                    self.airportSammy = self.airportAma?.Airport
                    
                    self.airportPosition = self.airportSammy?.Position
                    
                    self.airportCoordinate = self.airportPosition?.Coordinate
                    
                    self.airportLatitude = self.airportCoordinate?.Latitude
                    self.airportLongitude = self.airportCoordinate?.Longitude
              
//                    print(self.airportLatitude)
//                    print(self.airportLongitude)
                    
                    
                    //run checks to see if airport latitude for either destination or origin are empty then assign accordingly!
                    
                    if self.airportLatitudeOrigin == nil {
                        
                        //assign latitude and logitude specific to origin airport to specific variables
                        self.airportLatitudeOrigin = self.airportLatitude
                        self.airportLongitudeOrigin = self.airportLongitude
                        
                    }else{
                        
                        //assign latitude and logitude specific to origin airport to specific variables
                        self.airportLatitudeDestination = self.airportLatitude
                        self.airportLongitudeDestination = self.airportLongitude
                        
                        
                    }
                    
                    
                }catch{
                    
                    print(error)
                    
                }
                
                
            }else{
                
                print(error!)
                
            }
            
            }.resume()
        
        
    }
    func getOriginPosition() {
        //empty these variables coz they need to be empty in function to get airports
        airportLatitude = nil
        airportLongitude = nil
        
        //assign airport code based on this function
        airportCode = originVar
        
       
        
        //assign origin airport to airport code
        self.getAirportDetails()
      
        
      
    }
    
    func getDestinationPosition() {
        //empty these variables coz they are needed in function to get airports
        airportLatitude = nil
        airportLongitude = nil
        
        //assign airport code based on this function
        airportCode = destinationVar
        
        
        //assign origin airport to airport code
        self.getAirportDetails()
        
        
      
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
    }
   
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? MapViewController {
            
       
            destination.airportLatitudeOrigin = airportLatitudeOrigin
            destination.airportLongitudeOrigin = airportLongitudeOrigin
            
            
            destination.airportLatitudeDestination = airportLatitudeDestination
            destination.airportLongitudeDestination = airportLongitudeDestination
       
            
        }
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}


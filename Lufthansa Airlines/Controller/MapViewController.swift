//
//  MapViewController.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 14/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var mapKitView: MKMapView!
    
    //prepare for values passed from previous view
    var airportLatitudeOrigin : Double?
    var airportLongitudeOrigin : Double?
    
    var airportLatitudeDestination : Double?
    var airportLongitudeDestination : Double?
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //request for authorization from user to use location
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()

        mapKitView.delegate = self
        mapKitView.showsScale = true
        mapKitView.showsPointsOfInterest = true
        mapKitView.showsUserLocation = true
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
        
        let originCoordinates = CLLocationCoordinate2DMake(airportLatitudeOrigin!, airportLongitudeOrigin!)
        let destinationCoordinates = CLLocationCoordinate2DMake(airportLatitudeDestination!, airportLongitudeDestination!)
      
        
        let sourcePlaceMark = MKPlacemark(coordinate: originCoordinates)
        let destiPlaceMark = MKPlacemark(coordinate: destinationCoordinates)
        
        let sourceItem = MKMapItem(placemark: sourcePlaceMark)
        let destiItem = MKMapItem(placemark: destiPlaceMark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceItem
        directionRequest.destination = destiItem
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate(completionHandler: {
            response, error in
            
            guard let response = response else {
                
                if let error = error {
                    print(error)
                    
                    let failAlert = UIAlertController(title: "Error", message: "Directions are not available between these locations", preferredStyle: UIAlertControllerStyle.alert)
                    
                    failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        failAlert.dismiss(animated: true, completion: nil)
                        
                        
                    }))
                    
                    DispatchQueue.main.async(execute: {
                        self.present(failAlert, animated: true, completion: nil)
                    })
                    
                    
                }
                return
            }
            
            let route = response.routes[0]
            self.mapKitView.add(route.polyline, level: .aboveRoads)
            
            let rekt = route.polyline.boundingMapRect
            
            self.mapKitView.setRegion(MKCoordinateRegionForMapRect(rekt), animated: true)
            
            
            
            
        })
        
//        print(airportLongitudeOrigin)
//        print(airportLatitudeOrigin)
//
//          print(airportLatitudeDestination)
//        print(airportLongitudeDestination)
        
        // Do any additional setup after loading the view.
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 5.0
        
        return renderer
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

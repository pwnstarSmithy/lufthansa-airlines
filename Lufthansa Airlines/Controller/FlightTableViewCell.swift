//
//  FlightTableViewCell.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 13/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class FlightTableViewCell: UITableViewCell {

    @IBOutlet weak var FlightName: UILabel!
    
    @IBOutlet weak var flightDuration: UILabel!
    
    @IBOutlet weak var flightStops: UILabel!
    
    @IBOutlet weak var flightDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

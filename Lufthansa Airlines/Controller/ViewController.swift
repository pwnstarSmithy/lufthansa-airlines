//
//  ViewController.swift
//  Lufthansa Airlines
//
//  Created by pwnstarSmithy on 10/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

//empty variables to save info
var airportsArray : AirportDetails?
var airportDummy : AirportsData?
var airportInfo = [Airport]()
class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    //variables to be passed onto next view
    var originCode : String?
    var destinationCode : String?
    
    //assign parameters to be used later for access token call
    var clientID = "mmfd68tzenrma8ay6hv44uhp"
    var clientSecret = "gnyVsSG9Md"
    var grantType = "client_credentials"
    
    
    
    
    @IBOutlet weak var originPicker: UIPickerView!
    
    @IBOutlet weak var destinationPicker: UIPickerView!
    
    @IBOutlet weak var originPickedLabel: UILabel!
    
    @IBOutlet weak var destinationPickedLabel: UILabel!
    
    @IBAction func checkPressed(_ sender: Any) {
        
        if (originPickedLabel!.text?.isEmpty)! || (destinationPickedLabel.text?.isEmpty)! {
            
            let failAlert = UIAlertController(title: "Error", message: "Please choose an airport", preferredStyle: UIAlertControllerStyle.alert)
            
            failAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                
                failAlert.dismiss(animated: true, completion: nil)
                
                
            }))
            
            DispatchQueue.main.async(execute: {
                self.present(failAlert, animated: true, completion: nil)
            })
            
        }else{
            
            originCode = originPickedLabel.text
            destinationCode = destinationPickedLabel.text
            
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
      getAccessToken()
        
        //connect destination picker
        self.destinationPicker.delegate = self
        self.destinationPicker.dataSource = self
        
        //connect origin picker
        self.originPicker.dataSource = self
        self.originPicker.delegate = self
      
        //add notifications observers for picker views, theese do something when picker view changes
        NotificationCenter.default.addObserver(self, selector: #selector(pickerChanged), name: Notification.Name("pickersChanged"), object: nil)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @objc func pickerChanged() {
        
        let start = originPicker.selectedRow(inComponent: 0)
        let end = destinationPicker.selectedRow(inComponent: 0)
        
        originPickedLabel.text = airportInfo[start].AirportCode
        destinationPickedLabel.text = airportInfo[end].AirportCode
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Function to make a custom request for access token by using POST
    func getAccessToken(){
        
        //send request to server
        let url = NSURL(string: "https://api.lufthansa.com/v1/oauth/token")!
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        let body = "client_id=\(clientID)&client_secret=\(clientSecret)&grant_type=\(grantType)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        //launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{return}
            // check if no error
            if error == nil{
                
                do{
                    
                    let tokenDetails = try JSONDecoder().decode(token.self, from: data)
                    
                    accessToken = tokenDetails.access_token
                    
                    //save generated token to UserDefaults, this saves it onto users phone
                    UserDefaults.standard.set(accessToken, forKey: "Token")
                    
                    print(tokenDetails.access_token)
                    
                    self.getAirports()
                    
                }catch{
                    //print error if any
                    print(error)
                    
                }
            }else{
                return
            }
        }.resume()
    }
    func getAirports(){
        
        //URL to send request to
        let url = NSURL(string: "https://api.lufthansa.com/v1/references/airports/?limit=100")!
        
        //append access token to bearer string
        let tokenString = "Bearer " + accessToken!
        
        //assign URL to request variable
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "GET"
        
        //set authorization parameter in authorization header
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
      request.setValue("application/json", forHTTPHeaderField: "Accept")
        //launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
          
            guard let data = data else {return}
            //check for any errors
            if error == nil {
                do{
                    let airportDetails = try JSONDecoder().decode(AirportData.self, from: data)
                    
                    airportsArray = airportDetails.AirportResource
                    
                   airportDummy = airportsArray?.Airports
                    
                    airportInfo = (airportDummy?.Airport)!
                    
                    DispatchQueue.main.async {
                    self.originPicker.reloadAllComponents()
                    self.destinationPicker.reloadAllComponents()
                    }
//                    print(airportsArray!)
                    
                }catch{
                    print("it catches an error")
                    print(error)
                }
            }else{
                
            }
            
        }.resume()
        
        
        
 
        
        
    }
    
    
    //functions to populate the picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return airportInfo.count
    }

    //create a custom view inside each component
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        //customize top Label
        let topLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 15))
        topLabel.text = airportInfo[row].TimeZoneId
        topLabel.textColor = UIColor.white
        topLabel.textAlignment = .center
        topLabel.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.thin)
        view.addSubview(topLabel)
        
        //customize Middle Label
        let midLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        midLabel.text = airportInfo[row].AirportCode
        midLabel.textColor = UIColor.white
        midLabel.textAlignment = .center
        midLabel.font = UIFont.systemFont(ofSize: 42, weight: UIFont.Weight.thin)
        view.addSubview(midLabel)
        
        //customize Bottom Label
        let botLabel = UILabel(frame: CGRect(x: 0, y: 78, width: 100, height: 15))
        botLabel.text = airportInfo[row].CountryCode
        botLabel.textColor = UIColor.white
        botLabel.textAlignment = .center
        botLabel.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.thin)
        view.addSubview(botLabel)
        
        
        return view
        
    }
    
    //function to assign height to each component in picker
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat{
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        NotificationCenter.default.post(name: Notification.Name("pickersChanged"), object: self)
    }

    //function to pass data onto next segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SecondViewController {
            
            destination.originVar = originCode
            destination.destinationVar = destinationCode
            
        }
    }
    
}

